@extends('home')
@section('section')
<div class="default-tab">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="payment-list-tab" data-toggle="tab" href="#payment-list" role="tab" aria-controls="payment-list"
        aria-selected="true" ><i class="fas fa-list"></i> All Payments</a>
  
        <a class="nav-item nav-link" id="lab-bill-tab" data-toggle="tab" href="#lab-bill" role="tab" aria-controls="lab-bill"
        aria-selected="false"><i class="fas fa-list"></i> Lab Queue Billing</a>
  
        <a class="nav-item nav-link" id="patient-bill-tab" data-toggle="tab" href="#patient-bill" role="tab" aria-controls="patient-bill"
        aria-selected="false"><i class="fas fa-list"></i> Patient Billings</a>
      </div>
    </nav>
    <div class="tab-content pl-3 pt-2" id="nav-tabContent">
  
      <div class="tab-pane fade show active" id="payment-list" role="tabpanel" aria-labelledby="payment-list-tab">
        @include('billings.list')
      </div>
  
      <div class="tab-pane fade" id="lab-bill" role="tabpanel" aria-labelledby="lab-bill-tab">
        @include('billings.lab-bill')
      </div>

      <div class="tab-pane fade" id="patient-bill" role="tabpanel" aria-labelledby="patient-bill-tab">
        @include('billings.patient-bill')
      </div>
  
    </div>
  </div>
  @endsection