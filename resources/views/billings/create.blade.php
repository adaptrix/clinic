<!-- modal edit Category -->
<div class="modal fade" id="payBill" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="largeModalLabel">Make Payment</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
            <form id="billing-create" enctype="multipart/form-data" method="post" action="{{ route('billings.store') }}" >
                {{ csrf_field() }}
				<div class="modal-body">
					<div class="row form-group">
						<div class="col-md-6">
							<p>Transaction ID: <span></span></p>
							<input type="text" name="transaction_id" id="transaction_id" hidden>
                        </div>
                        <div class="col-md-6">
							<p>Patient Reg. #: <span></span></p>
							<input type="text" name="patient_reg_no" id="patient_reg_no" hidden>
                        </div>
                        <div class="col-md-12">
							<p>Requested Service: <span></span></p>
							<input type="text" name="service_id" id="service_id" hidden>
						</div>
                        <div class="col-md-12">
							<p>Service Charge: <span></span></p>
							<input type="text" name="amount" id="amount" hidden>
						</div>
						<br/><br/><br/>
						<div class="col-md-6">
							<div class="form-group">
                                <label for="description" class=" form-control-label">Amount</label>
                                <div class="input-group">
                                    <div class="input-group-addon">TZS
                                    </div>
                                    <input type="number" id="amount_paid" name="amount_paid" class="form-control" value="0" min="0">
                                </div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="description" class=" form-control-label">Discount</label>
								<div class="input-group">
									<div class="input-group-addon">TZS
									</div>
									<input type="number" id="discount" name="amount" class="form-control" value="0" min="0">
								</div>
							</div>
						</div>						
					</div>
					<br>
					{{-- <strong class="pull-right text-right">Amount Paid: TZS 7900</strong><br />
					<strong class="pull-right text-right">Amount Due: TZS 7900</strong><br /> --}}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Confirm</button>
				</div>
            </form>
		</div>
	</div>
</div>
<!-- end edit Category -->
