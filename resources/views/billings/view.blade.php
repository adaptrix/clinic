<!-- modal edit Category -->
<div class="modal fade" id="editMedCategory" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="largeModalLabel">Payment Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
            <form enctype="multipart/form-data" method="post" action="" >
                {{ csrf_field() }}
				<div class="modal-body">
					<div class="row form-group">
                        <div class="col-md-4">
                            <p>Date: <span></span></p>
                        </div>
						<div class="col-md-4">
							<p>Transaction ID: <span></span></p>
                        </div>
                        <div class="col-md-4">
                            <p>Requested Service: <span></span></p>
                        </div>
                        <div class="col-md-6">
							<p>Patient Reg. #: <span></span></p>
                        </div>
                        <div class="col-md-6">
                            <p>Patient Name: <span></span></p>
                        </div>
                        
                        <strong class="pull-right">Amount Due: TZS 7900</strong><br />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Confirm</button>
				</div>
            </form>
		</div>
	</div>
</div>
<!-- end edit Category -->
