  <!-- DATA TABLE-->
  <section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Patient Medicine &amp; Service Payments</h3>

                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-billing" class="table table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reg. #</th>
                                        <th>Name</th>      
                                        <th>Service Requested</th>
                                        <th>Doctor</th>
                                        <th>Amount</th>
                                        <th>Amount Paid</th>
                                        <th>Amount Due</th>
                                        <th>Discount</th>
                                        <th>Waiting Time</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->
