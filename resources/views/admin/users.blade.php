@extends('layouts.admin')
@section('content')
      <!-- DATA TABLE-->
  <section class="p-t-20">
        <div class="container">
            <div class="row">
                    
                <div class="col-md-12">
                    <a href="#" class="pull-right" data-target="#userModal" data-toggle="modal">
                        <button class="btn btn-primary"><i class="fas fa-plus"> New User</i></button>
                    </a>
                    <h3 class="title-5 m-b-35">Users</h3>                    
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table id="users-table" class="table table-borderless table-data3">
                                    <thead>
                                        <tr>
                                            <th>Employee ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE                  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
    @include('admin.modal.users')
@endsection