<div class="modal fade" id="roomModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Rooms</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="rooms-form" enctype="multipart/form-data" method="post" action="{{ url('/admin/room/create') }}" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="room_id" hidden>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="room_no" class=" form-control-label">Room</label>
                                <div class="input-group">
                                    {{-- <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div> --}}
                                    <input type="text" name="room_no" id="room_no" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="use" class=" form-control-label">Use</label>
                                <div class="input-group">
                                    {{-- <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div> --}}
                                    <input type="text" name="use" id="use" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit Category -->
