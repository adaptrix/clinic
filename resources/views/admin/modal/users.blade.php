<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="user-form" enctype="multipart/form-data" method="post" action="{{ url('/admin/user/create') }}" >
                {{ csrf_field() }}
                <input type="text" name="id" id="user-id" hidden>
                <div class="modal-body">

                    <div class="row form-group">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="employee_id" class=" form-control-label">Employee No.</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div>
                                    <input type="text" name="employee_id" id="employee_id" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class=" form-control-label">Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email" class=" form-control-label">Email</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-at"></i>
                                    </div>
                                    <input type="email" name="email" id="email" class="form-control" required>
                                </div>
                            </div>
                        </div>    
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role" class=" form-control-label">User Role</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <select name="role" id="role" class="form-control" required>
                                        <option value="">Please select...</option>
                                        <option value="receptionist">Receptionist</option>
                                        <option value="laboratorist">Laboratorist</option>
                                        <option value="pharmacist">Pharmacist</option>
                                        <option value="doctor">Doctor</option>
                                        <option value="admin">Administrator</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />

                    <div class="row form-group">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password" class=" form-control-label">Password</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                    <input type="password" id="password" name="password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="passconfrim" class=" form-control-label">Confirm Password</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                    <input type="password" id="passconfirm" name="passconfirm" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>