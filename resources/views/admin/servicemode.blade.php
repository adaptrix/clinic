@extends('layouts.admin')
@section('content')
      <!-- DATA TABLE-->
  <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="#" class="pull-right" data-target="#servicemodeModal" data-toggle="modal">
                        <button class="btn btn-primary"><i class="fas fa-plus"> New Service Mode</i></button>
                    </a>                    
                    <h3 class="title-5 m-b-35">Services Modes</h3>  
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table id="servicemodes-table" class="table table-borderless table-data3">
                                    <thead>
                                        <tr>
                                            <th>Service Mode</th>
                                            <th>Price</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE                  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
    @include('admin.modal.servicemode')
  
@endsection