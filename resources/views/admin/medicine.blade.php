@extends('layouts.admin')
@section('content')
    @include('medicine.list')

    @include('medicine.edit')
    @include('medicine.category-edit')
@endsection