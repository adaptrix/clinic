@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span30">
        <!--big normal buttons-->
        <div class="action-nav-normal">
            <div class="row-fluid">
              @if( Auth::user()->hasRole('receptionist') )
                <div class="span2 action-nav-button">
                  <a href="{{ url('/reception/billing') }}">
                    <i class="fas fa-credit-card"></i>
                    <span>Payments</span>
                  </a>
                </div>
                <div class="span2 action-nav-button">
                  <a href="{{url('/reception/appointments')}}">
                    <i class="fas fa-calendar"></i>
                    <span>Appointments</span>
                  </a>
                </div>
                <div class="span2 action-nav-button">
                  <a href="{{url('/reception/patients')}}">
                    <i class="fas fa-user"></i>
                    <span>Registration</span>
                  </a>
                </div>
              @endif
              @if(Auth::user()->hasRole('pharmacist'))
                <div class="span2 action-nav-button">
                  <a href="{{ url('/pharmacy') }}">
                    <i class="fas fa-medkit"></i>
                    <span>List of Medicines</span>
                  </a>
                </div>
                <div class="span2 action-nav-button">
                  <a href="{{url('/pharmacy/prescription')}}">
                    <i class="fas fa-user-md"></i>
                    <span>Prescriptions</span>
                  </a>
                </div>
                <div class="span2 action-nav-button">
                  <a href="{{ url('/pharmacy/billings') }} ">
                    <i class="fas fa-credit-card"></i>
                    <span>Billings</span>
                  </a>
                </div>
              @endif
              @if(Auth::user()->hasRole('laboratorist'))
              
              @endif
              @if(Auth::user()->hasRole('doctor'))
                <div class="span2 action-nav-button">
                  <a href="#">
                    <i class="fas fa-medkit"></i>
                    <span>Lab Results</span>
                  </a>
                </div>
                {{-- <div class="span2 action-nav-button">
                  <a href="">
                    <i class="fas fa-user-md"></i>
                    <span>Diagnosis</span>
                  </a>
                </div> --}}
                <div class="span2 action-nav-button">
                  <a href="{{ url('/doctor/appointments') }}">
                    <i class="fas fa-calendar"></i>
                    <span>Appoitments</span>
                  </a>
                </div>
                <div class="span2 action-nav-button">
                  <a href="{{ url('/doctor/patients') }}">
                    <i class="fas fa-user"></i>
                    <span>Patients</span>
                  </a>
                </div>
              @endif
            </div>
        </div>
    </div>
    <!---DASHBOARD MENU BAR ENDS HERE-->
</div>
<br />
<br />

@yield('section')

@if(Auth::user()->hasRole('receptionist') && Request::segment(2) == 'appointments')  
  @include('appointments.edit')
@endif
@if(Auth::user()->hasRole('pharmacist') && Request::segment(2) == 'billings')
  @include('billings.create')
  @include('billings.edit')
@endif
@if(Auth::user()->hasRole('pharmacist') && Request::segment(2) == '')
  @include('medicine.edit')
  @include('medicine.category-edit')
@endif

@endsection
