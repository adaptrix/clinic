<!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="#">
                <img src="images/icon/" alt="{{config('app.name')}}" />
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                    <li class="active">
                        <a href="{{url('/')}}">
                            <i class="fas fa-hospital-o"></i>Home</a>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-user"></i>Patient</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{url('/admin/patients/list')}}"><i class="fa fa-list-ol"></i>List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fa fa-flask"></i>Laboratory</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="#"><i class="fas fa-list"></i>Lab Queue</a>
                            </li>
                            <li>
                                <a href="#"><i class="fas fa-file"></i>Lab Results</a>
                            </li>
                            <li>
                                <a href="#"><i class="fas fa-send"></i>Lab Requests</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-medkit"></i>Pharmacy</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">  
                            <li>
                                <a href="#">List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-medkit"></i>Medicine</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{ url('/admin/medicine/list') }}"><i class="fas fa-list"></i>All Medicine</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-credit-card"></i>Payments</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{ url('/admin/billing/list') }}"><i class="fas fa-list-ol"></i>List All</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-calendar"></i>Appoinment</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{ url('/admin/appointment/upcoming') }}"><i class="fas fa-calendar-plus-o"></i>Upcoming</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/appointment/list') }}"><i class="fas fa-list-ol"></i>All Appointments</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-cogs"></i>Settings</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{ url('/admin/services') }}"><i class="fas fa-ambulance"></i>Services</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/servicemodes') }}"><i class="fas fa-file"></i>Service Modes</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/rooms') }}"><i class="fas fa-hospital"></i>Rooms</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/units') }}"><i class="fas fa-file"></i>Unit Allocation</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/users') }}"><i class="fas fa-users"></i>Users</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/settings') }}"><i class="fas fa-cog"></i>System Settings</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
<!-- END MENU SIDEBAR-->