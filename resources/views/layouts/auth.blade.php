<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Elijah Yoga">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title Page-->
    <title>{{ config('app.name') }}</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('assets/css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('assets/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('assets/vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet" media="all">

    <style>
        .has-error .help-block { color:#a94442; }
        .has-error .au-input
        {
            border-color:#a94442;
            box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
        }
        .has-error .au-input:focus
        {
            border-color:#843534;box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483;
        }
        .has-error .input-group-addon
        {
            color:#a94442;border-color:#a94442;
            background-color:#f2dede;
        }
        .help-block { color:#8a6d3b;}
        .help-block
        {
            display:block;
            margin-top:5px;
            margin-bottom:10px;
            color:#a4aaae;
        }
        .alert { margin-bottom:22px; }

        .alert{ padding:15px;border:1px solid transparent;border-radius:4px; }
        .alert>p { margin-bottom:0; }
        .alert>p+p { margin-top:5px; }
        .alert-success { background-color:#dff0d8;border-color:#d6e9c6;color:#3c763d; }
    </style>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/logo.png" alt="{{ config('app.name') }}">
                            </a>
                        </div>
                        <div class="login-form">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('assets/vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('assets/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{ asset('assets/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{ asset('assets/vendor/wow/wow.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{ asset('assets/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{ asset('assets/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{ asset('assets/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.min.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{ asset('assets/js/main.js')}}"></script>

</body>

</html>
<!-- end document-->