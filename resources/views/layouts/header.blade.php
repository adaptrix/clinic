<header class="header-desktop3 d-none d-lg-block">
     <div class="section__content section__content--p35">
         <div class="header3-wrap">
             <div class="header__logo">
                 <a href="#">
                     <img src="images/icon/logo-white.png" alt="{{config('app.name')}}" />
                 </a>
             </div>
             <div class="header__navbar">
                 <ul class="list-unstyled">
                    @if(Auth::user()->hasRole('receptionist'))
                     <li>
                         <a href="{{url('/')}}">
                             <i class="fas fa-hospital-o"></i>
                             <span class="bot-line"></span>Reception</a>
                     </li>
                    @endif
                    @if(Auth::user()->hasRole('receptionist') || Auth::user()->hasRole('doctor'))
                     <!--<li class="has-sub">
                         <a href="#">
                             <i class="fas fa-user"></i>Patient
                             <span class="bot-line"></span>
                         </a>
                         <ul class="header3-sub-list list-unstyled">
                             <li>
                                 <a href="{{url('/')}}#patient-queue">Patient Queue</a>
                             </li>
                             <li>
                                 <a href="{{url('/list')}}">View Patients</a>
                             </li>
                            @if(Auth::user()->hasRole('receptionist'))
                             <li>
                                 <a href="{{url('/new')}}">Register New</a>
                             </li>
                             <li>
                                 <a href="{{url('/visit')}}">New Visit</a>
                             </li>
                            @endif
                         </ul>
                     </li>-->
                    @endif
                    @if(Auth::user()->hasRole('laboratorist'))
                     <li class="has-sub">
                         <a href="{{ url('/') }}">
                             <i class="fas fa-flask"></i>Lab
                             <span class="bot-line"></span>
                         </a>
                         {{-- <ul class="header3-sub-list list-unstyled">
                             <li>
                                 <a href="#">Lab Queue</a>
                             </li>
                             <li>
                                 <a href="#">Lab Results</a>
                             </li>
                             <li>
                                 <a href="#">Lab Requests</a>
                             </li>
                         </ul> --}}
                     </li>
                    @endif
                    @if(Auth::user()->hasRole('pharmacist'))
                     <li>
                         <a href="{{url('/pharmacy')}}">
                             <i class="fas fa-medkit"></i>
                             <span class="bot-line"></span>Pharmacy</a>
                     </li>
                     <!--<li class="has-sub">
                         <a href="#">
                             <i class="fas fa-medkit"></i>Medicine
                             <span class="bot-line"></span>
                         </a>
                         <ul class="header3-sub-list list-unstyled">
                             <li>
                                 <a href="{{url('/list-med')}}">All Medicines</a>
                             </li>
                             <li>
                                 <a href="#" data-toggle="modal" data-target="#addMedicine">Add Medicine</a>
                             </li>
                             <li>
                                 <a href="{{url('/cat-med')}}" >Medicine Category</a>
                             </li>
                             <li>
                                 <a href="#" data-toggle="modal" data-target="#medCategory">Add Category</a>
                             </li>
                         </ul>
                     </li>-->
                    @endif
                    @if(Auth::user()->hasRole('receptionist') || Auth::user()->hasRole('doctor'))
                     <!--<li class="has-sub">
                         <a href="#">
                             <i class="fas fa-calendar"></i>Appointments
                             <span class="bot-line"></span>
                         </a>
                         <ul class="header3-sub-list list-unstyled">
                             <li>
                                 <a href="{{url('/upcoming')}}">Upcoming</a>
                             </li>
                             <li>
                                 <a href="{{url('/')}}#appointments">View All</a>
                             </li>
                             @if(Auth::user()->hasRole('receptionist'))
                             <li>
                                 <a href="#" data-toggle="modal" data-target="#addAppointment">Add New</a>
                             </li>
                             @endif
                         </ul>
                     </li>-->
                     @endif
                 </ul>
             </div>
             <div class="header__tool">
                 <div class="header-button-item has-noti js-item-menu">
                     <i class="zmdi zmdi-notifications"></i>
                     <div class="notifi-dropdown notifi-dropdown--no-bor js-dropdown">
                         
                     </div>
                 </div>
                 <div class="account-wrap">
                     <div class="account-item account-item--style2 clearfix js-item-menu">
                         <div class="image">
                             <img src="assets/images/avatar/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" />
                         </div>
                         <div class="content">
                             <a class="js-acc-btn" href="#">{{ Auth::user()->name }}</a>
                         </div>
                         <div class="account-dropdown js-dropdown">
                             <div class="info clearfix">
                                 <div class="image">
                                     <a href="#">
                                         <img src="assets/images/avatar/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" />
                                     </a>
                                 </div>
                                 <div class="content">
                                     <h5 class="name">
                                         <a href="#">{{ Auth::user()->name }}</a>
                                     </h5>
                                     <span class="email">{{ Auth::user()->email }}</span>
                                 </div>
                             </div>
                             <div class="account-dropdown__body">
                                 <div class="account-dropdown__item">
                                     <a href="#">
                                         <i class="zmdi zmdi-account"></i>Account</a>
                                 </div>
                             </div>
                             <div class="account-dropdown__footer">
                                 <a href="{{ url('logout') }}">
                                     <i class="zmdi zmdi-power"></i>Logout</a>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </header>
