@extends('home')
@section('section')

    <div class="default-tab">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            @if(Auth::user()->hasRole('receptionist'))
            <a class="nav-item nav-link active" id="patient-list-tab" data-toggle="tab" href="#patient-list" role="tab" aria-controls="patient-list"
             aria-selected="true" ><i class="fas fa-list"></i> Patients List</a>

            <a class="nav-item nav-link" id="patient-visit-tab" data-toggle="tab" href="#patient-visit" role="tab" aria-controls="patient-visit"
             aria-selected="false"><i class="fas fa-plus"></i> Returning Patient</a>
      
            <a class="nav-item nav-link" id="adult-new-patient-tab" data-toggle="tab" href="#adult-new-patient" role="tab" aria-controls="adult-new-patient"
             aria-selected="false"><i class="fas fa-plus"></i> Adult New Patient</a>
      
            <a class="nav-item nav-link" id="child-new-patient-tab" data-toggle="tab" href="#child-new-patient" role="tab" aria-controls="child-new-patient"
             aria-selected="false"><i class="fas fa-plus"></i> Child New Patient</a>
            @endif
      
            <a class="nav-item nav-link @if(Auth::user()->hasRole('doctor')) active @endif" id="registered-patient-tab" data-toggle="tab" href="#registered-patient" role="tab" aria-controls="registered-patient"
             aria-selected="false"><i class="fas fa-list"></i> Registered Patients</a>
            
          </div>
        </nav>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
      
          @if(Auth::user()->hasRole('receptionist'))
          <div class="tab-pane fade show active" id="patient-list" role="tabpanel" aria-labelledby="patient-list-tab">
            @include('patients.queue')
          </div>
      
          <div class="tab-pane fade" id="patient-visit" role="tabpanel" aria-labelledby="patient-visit-tab">
            @include('patients.visit-form')
          </div>
      
          <div class="tab-pane fade" id="adult-new-patient" role="tabpanel" aria-labelledby="adult-new-patient-tab">
            @include('patients.create-adult')
          </div>
      
          <div class="tab-pane fade" id="child-new-patient" role="tabpanel" aria-labelledby="child-new-patient-tab">
            @include('patients.create-child')
          </div>
          @endif

          <div class="tab-pane fade @if(Auth::user()->hasRole('doctor')) show active @endif" id="registered-patient" role="tabpanel" aria-labelledby="redgistered-patient-tab">
            @include('patients.registered')
          </div>
      
        </div>
    </div>      
    <div class="captured_image"></div>
    <div class="child-captured_image"></div>
@endsection