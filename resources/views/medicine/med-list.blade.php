<!-- modal edit Category -->
<div class="modal fade" id="medList" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Drug Issue</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Medicine Collection #: <strong> 0001</strong></p>
                <p>Date: <strong> {{ date('d/m/Y') }}</strong></p>
                <p>Patient Name: <strong> John Doe</strong></p>
                <p>Doctor Seen: <strong> John Doe</strong></p><br>
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE -->
                        <h4 class="title-5 m-b-35">Medicine Requested</h4>
                        
                        <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>type</th>
                                        <th>quantity</th>
                                        <th>dose</th>
                                        <th>price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tr-shadow">
                                        <td>Lori Lynch</td>
                                        <td>
                                            <span class="block-email">lori@example.com</span>
                                        </td>
                                        <td class="desc">Samsung S8 Black</td>
                                        <td>2018-09-27 02:12</td>
                                        <td>$679.00</td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit Category -->


