<!-- DATA TABLE-->
<section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Patients Queue</h3>

                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-que" class="table datatable-1 table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>Que #</th>
                                        <th>Date &amp; Time</th>
                                        <th>Patient Name</th>
                                        <th>Investigation</th>
                                        <th>Waiting Time</th>
                                        <th>Report</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>001</td>
                                        <td>2018-09-29 05:57</td>
                                        <td>P0019</td>
                                        <td>John Doe</td>
                                        <td>Insurance</td>
                                        <td>Insurance</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="btn btn-success"><i class="fa fa-flask"> Run Test</i></button>
                                                <button class="btn btn-danger"><i class="zmdi zmdi-delete"> Remove</i></button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->