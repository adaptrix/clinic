<!-- modal edit midicine -->
<div class="modal fade" id="editRequest" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Laboratory Request Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form enctype="multipart/form-data" method="post" action="" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">

                        <div class="col-md-12"><span>Date  {{ date('d-m-Y') }}</span></div>
                        <br/>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class=" form-control-label">Item Name</label>
                                <input type="text" name="item" id="name" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="quantity" class=" form-control-label">Quantity</label>
                                <input type="number" name="quantity" id="quantity" class="form-control" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit medicine -->
