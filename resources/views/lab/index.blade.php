@extends('home')
@section('section')

    <div class="table-data__tool-right pull-right">
        <a href="#" data-target="#createRequest" data-toggle="modal">
            <button class="au-btn au-btn-icon au-btn--blue au-btn--small"><i class="zmdi zmdi-plus"></i>Make Request</button>
        </a>
    </div>
    
    <div class="default-tab">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
    
                <a class="nav-item nav-link active" id="queue-list-tab" data-toggle="tab" href="#queue-list" role="tab" aria-controls="queue-list"
                aria-selected="true" ><i class="fas fa-list"></i> Lab Queue</a>
            
                <a class="nav-item nav-link" id="request-tab" data-toggle="tab" href="#request-list" role="tab" aria-controls="request-list"
                aria-selected="false"><i class="fas fa-list"></i> Requests</a>
    
                <a class="nav-item nav-link" id="result-tab" data-toggle="tab" href="#result-list" role="tab" aria-controls="result"
                aria-selected="false"><i class="fas fa-flask"></i> Results</a>
    
            </div>
        </nav>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
        
            <div class="tab-pane fade show active" id="queue-list" role="tabpanel" aria-labelledby="queue-list-tab">
                @include('lab.queue')
            </div>
        
            <div class="tab-pane fade" id="request-list" role="tabpanel" aria-labelledby="request-tab">
                @include('lab.requests')
            </div>
            
            <div class="tab-pane fade" id="result-list" role="tabpanel" aria-labelledby="result-tab">
                @include('lab.result')
            </div>
        
        </div>
    </div>

    @include('lab.request-create')
    @include('lab.request-edit')

@endsection