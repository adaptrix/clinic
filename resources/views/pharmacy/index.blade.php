@extends('home')
@section('section')
    
    <div class="default-tab">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="medicine-list-tab" data-toggle="tab" href="#medicine-list" role="tab" aria-controls="medicine-list"
            aria-selected="true" ><i class="fas fa-list"></i> Medicines</a>
      
            <a class="nav-item nav-link" id="categories-tab" data-toggle="tab" href="#categories" role="tab" aria-controls="categories"
            aria-selected="false"><i class="fas fa-list"></i> Categories</a>
      
            <a class="nav-item nav-link" id="new-medicine-tab" data-toggle="tab" href="#new-medicine" role="tab" aria-controls="new-medicine"
            aria-selected="false"><i class="fas fa-plus"></i> New Medicine</a>
      
            <a class="nav-item nav-link" id="new-category-tab" data-toggle="tab" href="#new-category" role="tab" aria-controls="new-category"
            aria-selected="false"><i class="fas fa-plus"></i> New Category</a>
      
            <a class="nav-item nav-link" id="stock-tab" data-toggle="tab" href="#stock" role="tab" aria-controls="stock"
            aria-selected="false"><i class="fas fa-list"></i> Items and Stock</a>
      
            <a class="nav-item nav-link" id="inventory-tab" data-toggle="tab" href="#inventroy" role="tab" aria-controls="inventory"
            aria-selected="false"><i class="fas fa-list"></i> Inventory</a>
          </div>
        </nav>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
      
          <div class="tab-pane fade show active" id="medicine-list" role="tabpanel" aria-labelledby="medicine-list-tab">
            @include('medicine.list')
          </div>
      
          <div class="tab-pane fade" id="categories" role="tabpanel" aria-labelledby="categories-tab">
            @include('medicine.category-list')
          </div>
      
          <div class="tab-pane fade" id="new-medicine" role="tabpanel" aria-labelledby="new-medicine-tab">
            @include('medicine.create')
          </div>
      
          <div class="tab-pane fade" id="new-category" role="tabpanel" aria-labelledby="new-category-tab">
            @include('medicine.category')
          </div>
      
          <div class="tab-pane fade" id="stock" role="tabpanel" aria-labelledby="stock-tab">
          </div>
      
          <div class="tab-pane fade" id="inventory" role="tabpanel" aria-labelledby="inventory-tab">
          </div>
      
        </div>
    </div>
    
@endsection