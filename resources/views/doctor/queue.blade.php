  <!-- DATA TABLE-->
  <section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Patients Queue</h3>

                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-doc-que" class="table table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reg. Time</th>
                                        <th>Que. #</th>
                                        <th>name</th>
                                        <th>Type</th>
                                        <th>Service Mode</th>
                                        <th>Waiting Time</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->