
<div class="default-tab">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="queue-list-tab" data-toggle="tab" href="#queue-list" role="tab" aria-controls="queue-list"
        aria-selected="true" ><i class="fas fa-list"></i> Patient Queue</a>
  
        <a class="nav-item nav-link" id="diagnosis-tab" data-toggle="tab" href="#diagnosis" role="tab" aria-controls="diagnosis"
        aria-selected="false"><i class="fas fa-list"></i> Diagnosis</a>
      </div>
    </nav>
    <div class="tab-content pl-3 pt-2" id="nav-tabContent">
  
      <div class="tab-pane fade show active" id="queue-list" role="tabpanel" aria-labelledby="queue-list-tab">
        @include('doctor.queue')
      </div>
  
      <div class="tab-pane fade" id="diagnosis" role="tabpanel" aria-labelledby="diagnosis-tab">
        @include('doctor.diagnosis')
      </div>
  
    </div>
  </div>