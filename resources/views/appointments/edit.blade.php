<!-- modal edit appointment -->
<div class="modal fade" id="editAppointment" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="largeModalLabel">Edit Appointment</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
            <form enctype="multipart/form-data" method="post" action="" >
                {{ csrf_field() }}
			    <div class="modal-body">

                    <div class="row form-group">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="patient_reg_no" class=" form-control-label">Patient Reg. No.</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div>
                                    <input type="text" name="patient_reg_no" id="patient_reg_no" class="form-control" required disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name" class=" form-control-label">Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" name="name" id="name" class="form-control" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="gender" class=" form-control-label">Gender</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-intersex"></i>
                                    </div>
                                    <input type="text" id="gender" name="gender" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email" class=" form-control-label">Email</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-at"></i>
                                    </div>
                                    <input type="email" name="email" id="email" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address" class=" form-control-label">Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" name="address" id="address" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="phone" class=" form-control-label">Mobile Phone No.</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile"></i>
                                    </div>
                                    <input type="text" name="phone" id="phone" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="type" class=" form-control-label">Type of Patient</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-hospital"></i>
                                    </div>
                                    <select name="type" id="type" class="form-control">
                                        <option value="0">Please select</option>
                                        <option value="General">General</option>
                                        <option value="Corporate">Corporate</option>
                                        <option value="Insurance">Insurance</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row form-group">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="dob" class=" form-control-label">Date &amp; Time of Appointment</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" id="dob" name="dob" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="service_id" class=" form-control-label">Requested Service</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-hand-stop-o"></i>
                                    </div>
                                    <select name="service_id" id="service_id" class="form-control">
                                        <option value="0">Please select</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="doc_id" class=" form-control-label">Assign Doctor</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </div>
                                    <select name="doc_id" id="doc_id" class="form-control">
                                        <option value="0">Please select</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <strong class="pull-right" id="display-amount">Amount Due: TZS 7900</strong><br />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
		</div>
	</div>
</div>
<!-- end edit appointment -->
