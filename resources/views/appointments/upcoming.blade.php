
  <!-- DATA TABLE-->
  <section class="p-t-20">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h3 class="title-5 m-b-35">Today's Appointments</h3>

                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="upcoming-que" class="table table-borderless table-data3">
                                  <thead>
                                      <tr>
                                          <th>Que #</th>
                                          <th>Date &amp; Time</th>
                                          <th>name</th>
                                          <th>Allocated Doctor</th>
                                          <th>Room #</th>
                                          <th>Checked In</th>
                                          <th>Consultation Charge</th>
                                          <th>Advance Pay</th>
                                          <th>Amount Due</th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- END DATA TABLE-->