@extends('home')
@section('section')

<div class="default-tab">
  <nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a class="nav-item nav-link active" id="appointments-tab" data-toggle="tab" href="#appointments" role="tab" aria-controls="appointments"
       aria-selected="true" ><i class="fas fa-list"></i> All Appointments</a>
      <a class="nav-item nav-link" id="upcoming-appointment-tab" data-toggle="tab" href="#upcoming-appointment" role="tab" aria-controls="upcoming-appointment"
       aria-selected="false"><i class="fas fa-list"></i> Today's Appointments</a>
      @if(Auth::user()->hasRole('receptionist'))
      <a class="nav-item nav-link" id="new-appointment-tab" data-toggle="tab" href="#new-appointment" role="tab" aria-controls="new-appointment"
        aria-selected="false"><i class="fas fa-plus"></i> New Appointment</a>
      @endif
    </div>
  </nav>
  <div class="tab-content pl-3 pt-2" id="nav-tabContent">

    <div class="tab-pane fade show active" id="appointments" role="tabpanel" aria-labelledby="appointments-tab">
      @include('appointments.list')
    </div>

    <div class="tab-pane fade" id="upcoming-appointment" role="tabpanel" aria-labelledby="upcoming-appointment">
      @include('appointments.upcoming')
    </div>
    
    @if(Auth::user()->hasRole('receptionist'))
    <div class="tab-pane fade" id="new-appointment" role="tabpanel" aria-labelledby="new-appointment-tab">
      @include('appointments.create')
    </div>
    @endif

  </div>
</div>

@endsection
