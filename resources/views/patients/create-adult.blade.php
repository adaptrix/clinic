
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong><i class="fa fa-user"></i>   Adult Patient Registration</strong>
                        <small> Form</small>
                    </div>
                    <form enctype="multipart/form-data" method="post" action="{{route('patients.store')}}" id="register-new-patient">
                        {{ csrf_field() }}
                        <input type="text" name="patient_type" id="patient_type" value="adult" hidden>
                        <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="reg_no" class=" form-control-label">Patient Reg No.</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-id-card"></i>
                                        </div>
                                        <input type="text" name="reg_no" id="adult_reg_no" class="form-control"  disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name" class=" form-control-label">Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" name="name" id="name" class="form-control"  autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="gender" class=" form-control-label">Gender</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-intersex"></i>
                                        </div>
                                        <select name="type" id="type" class="form-control" >
                                            <option value="0">Please select</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="blood_group" class=" form-control-label">Blood Group</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-tint"></i>
                                        </div>
                                        <input type="text" id="blood_group" name="blood_group" class="form-control">
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="type" class=" form-control-label">Type of Patient</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-hospital"></i>
                                        </div>
                                        <select name="type" id="type" class="form-control" >
                                            <option value="">Please select</option>
                                            <option value="General">General</option>
                                            <option value="Corporate">Corporate</option>
                                            <option value="Insurance">Insurance</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="nationality" class=" form-control-label">Nationality</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-globe"></i>
                                        </div>
                                        <input type="text" name="nationality" id="nationality" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="occupation" class=" form-control-label">Occupation</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-briefcase"></i>
                                        </div>
                                        <input type="text" name="occupation" id="occupation" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="photo" class=" form-control-label">Patient's Photo</label>
                                    <div id="img-display"></div>                                
                                    <button id="capturePhoto" class="btn btn-primary"><i class="fa fa-camera"></i> Take a photo</button>
                                    <p>OR Upload</p>
                                    <input type="file" id="photo" name="photo" class="form-control-file">
                                </div>
                            </div>
                        </div>

                        <p><span>Contacts</span></p>
                        <hr />

                        <div class="row form-group">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="email" class=" form-control-label">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-at"></i>
                                        </div>
                                        <input type="email" name="email" id="email" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="address" class=" form-control-label">Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <input type="text" name="address" id="address" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="phone" class=" form-control-label">Mobile Phone No.</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-mobile"></i>
                                        </div>
                                        <input type="text" name="phone" id="phone" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="landline" class=" form-control-label">Landline No.</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="text" name="landline" id="landline" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p><span>Date of Birth &amp; Age</span></p>
                        <hr />

                        <div class="row form-group">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="dob" class=" form-control-label">Date of Birth</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="date" id="adult-dob" name="dob" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="age-year" class=" form-control-label">Age</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="adult-year" name="age-year" class="form-control">
                                    </div>
                                    <small class="form-text text-muted">Year</small>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="age-year" class=" form-control-label"></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="adult-month" name="age-month" class="form-control">
                                    </div>
                                    <small class="form-text text-muted">Month</small>
                                </div>
                            </div>
                        </div>

                        <p><span>Vitals</span></p>
                        <hr />

                        <div class="row form-group">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="weight" class=" form-control-label">Weight</label>
                                    <div class="input-group">
                                        <input type="text" name="weight" id="weight" class="form-control" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="height" class=" form-control-label">Height</label>
                                    <div class="input-group">
                                        <input type="text" name="height" id="height" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="blood_pressure" class=" form-control-label">Blood Pressure</label>
                                    <div class="input-group">
                                        <input type="text" name="blood_presure" id="blood_pressure" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="pulse-rate" class=" form-control-label">Pulse Rate</label>
                                    <div class="input-group">
                                        <input type="text" name="pulse_rate" id="pulse_rate" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="respiratory_rate" class=" form-control-label">Respiratory Rate</label>
                                    <div class="input-group">
                                        <input type="text" name="respiratory_rate" id="respiratory_rate" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="temprature" class=" form-control-label">Temperature</label>
                                    <div class="input-group">
                                        <input type="text" name="temperature" id="temperature" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="other" class=" form-control-label">Other</label>
                                    <div class="input-group">
                                        <input type="text" name="other" id="other" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="nurse" class=" form-control-label">Name of Nurse</label>
                                    <div class="input-group">
                                        <input type="text" name="nurse" id="nurse" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>