
  <!-- DATA TABLE-->
  <section class="p-t-20">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h3 class="title-5 m-b-35">Registered Patients</h3>

                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="patient-list-table" class="table table-borderless table-data3">
                                  <thead>
                                      <tr>
                                        <th>reg. #</th>
                                        <th>name</th>
                                        <th>mobile #</th>
                                        <th>age</th>
                                        <th>type</th>
                                        <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- END DATA TABLE-->

  @push('script')
    @if(Auth::user()->hasRole('admin'))
    <script>
        var list_table = $('#patient-list');
        list_table.on('draw', function(event) {
            list_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                $(this.node()).find('.delete').on('click', function(event) {
                    event.preventDefault(); 
                    var link = $(this);                  
                    var id = link.data('id');

                    $.ajax({
                        url: "{{ url('patient/destroy') }}/" + id,
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                    })
                    .done(function() {
                        list_table.draw(false);
                    })
                    .fail(function() {
                        console.log("error");
                    });
                    
                });
            });
        });    
    </script>
    @endif
@endpush
