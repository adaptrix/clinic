@extends('home')
@section('section')
  <!-- DATA TABLE-->
  <section class="p-t-20">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <input type="text" name="p-id" id="p-id" value="{{ $patient->reg_no }}" hidden>
                  <h3 class="title-5 m-b-35">{{ $patient->reg_no }}:<strong> {{ $patient->name }}'s</strong> History</h3>

                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="patient-history" class="table datatable-1 table-borderless table-data3">
                                  <thead>
                                      <tr>
                                          <th>Date</th>
                                          <th>Requested Service</th>
                                          <th>Allocated Doctor</th>
                                          <th>Examination</th>
                                          <th>Differential diagnoses</th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- END DATA TABLE-->
  @endsection