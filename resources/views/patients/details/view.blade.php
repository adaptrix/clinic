@extends('home')
@section('section')
<section class="p-t-20">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <input type="text" name="p-id" id="p-id" value="{{ $patient->reg_no }}" hidden>
        <h3 class="title-5 m-b-35">{{ $patient->reg_no }}:<strong> {{ $patient->name }}'s</strong> Details</h3>

        <div class="row">
          <div class="col-md-4">
            <img src="assets/images/patients/{{ $patient->photo }}" alt="" width="200" height="auto">
          </div>

          <div class="row col-md-4">
            <div class="col-md-6">
              <p>Registration #</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->reg_no }}</p>
            </div>

            <div class="col-md-6">
              <p>Name</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->name }}</p>
            </div>

            <div class="col-md-6">
              <p>Gender</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->gender }}</p>
            </div>

            <div class="col-md-6">
              <p>Date of Birth</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->DOB->format('d-M-Y') }}</p>
            </div>

            <div class="col-md-6">
                <p>Age</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->DOB->age }}</p>
            </div>
          </div>

          <div class="row col-md-4">
            <div class="col-md-6">
              <p>Patient Type</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->type }}</p>
            </div>

            <div class="col-md-6">
              <p>Blood Group</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->blood_group }}</p>
            </div>

            <div class="col-md-6">
              <p>Nationality</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->nationality }}</p>
            </div>
            @if($patient->patient_type == 'adult')
            <div class="col-md-6">
              <p>Occupation</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->occupation }}</p>
            </div>
            @endif
          </div>
        </div>
        
        <br/><br/>
        <hr />

        <div class="row">
          @if($patient->patient_type == 'child')
          <div class="row col-md-6">
            <div class="col-md-12">
                <h4><strong>Parents</strong></h4><br/>
            </div>
            
            <div class="col-md-6">
              <p>Father's Name</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->father_name }}</p>
            </div>

            <div class="col-md-6">
              <p>Father's Occupation</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->father_occupation }}</p>
            </div>

            <div class="col-md-6">
              <p>Mother's Name</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->mother_name }}</p>
            </div>

            <div class="col-md-6">
              <p>Mother's Occupation</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->mother_occupation }}</p>
            </div>
          </div>
          @endif
          <div class="row col-md-6">
            <div class="col-md-12">
                <h4><strong>Contacts</strong></h4><br/>
            </div>
            <div class="col-md-6">
              <p>Mobile Phone</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->phone }}</p>
            </div>

            <div class="col-md-6">
              <p>Landline No.</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->telephone }}</p>
            </div>

            <div class="col-md-6">
              <p>Email</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->email }}</p>
            </div>

            <div class="col-md-6">
              <p>Address</p>
            </div>
            <div class="col-md-6">
              <p>{{ $patient->address }}</p>
            </div>
          </div>
        </div>
        <br/><br/>
        <hr />
                       
        <div class="col-md-12">
          <div class="default-tab">
                <nav>
                  <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    @if(Auth::user()->hasRole('doctor'))
                    <a class="nav-item nav-link active" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history"
                      aria-selected="true" ><i class="fas fa-folder"></i> Patient's History</a>
                    @endif
              
                    <a class="nav-item nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment"
                      aria-selected="false"><i class="fas fa-credit-card"></i> Payments</a>  
                  </div>
                </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
              @if(Auth::user()->hasRole('doctor'))
                <div class="tab-pane fade show active" id="history" role="tabpanel" aria-labelledby="history-tab">
                    @include('patients.details.history')
                </div>
              @endif
              
                <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                    @include('patients.details.payments')              
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  
@endsection