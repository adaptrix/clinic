
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong><i class="fa fa-user"></i>   Patient Visitation</strong>
                            <small> Form</small>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="{{ url('/patient/visit') }}" id="new-patient-visit">
                          {{ csrf_field() }}
                          <input type="text" class="check-id" id="check-id" name="id" hidden>
                          <div class="card-body card-block">
                              <div class="row form-group">
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="reg_no" class=" form-control-label">Patient Reg No.</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-id-card"></i>
                                              </div>
                                              <input type="text" name="reg_no" id="reg_no" class="form-control reg_no" required disabled>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="que_no" class=" form-control-label">Queue No.</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-sort-numeric-asc"></i>
                                              </div>
                                              <input type="text" name="que_no" id="que_no" class="form-control que_no" required disabled>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="name" class=" form-control-label">Name</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-user"></i>
                                              </div>
                                              <input type="text" name="name" id="check-name" class="form-control name" required autofocus>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="type" class=" form-control-label">Type of Patient</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-hospital"></i>
                                              </div>
                                              <input type="text" id="type" name="type" class="form-control type" disabled>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="service_mode_id" class=" form-control-label">Service Mode</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-ambulance"></i>
                                              </div>
                                              {{ Form::select('service_mode_id', $service_modes, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="file-input" class=" form-control-label">Patient's Photo</label>
                                          <img id="p-photo" src="" width="50" height="50" />
                                      </div>
                                  </div>
                              </div>

                              <hr />

                              <div class="row form-group">
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="service_id" class=" form-control-label">Requested Service</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-hand-stop-o"></i>
                                              </div>
                                              {{ Form::select('service_id', $services, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="doc_id" class=" form-control-label">Assign Doctor</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-user-md"></i>
                                              </div>
                                              {{ Form::select('doc_id', $doctors, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="room_id" class=" form-control-label">Room No.</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-building"></i>
                                              </div>
                                              {{ Form::select('room_no', $rooms, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                                          <label for="unit_id" class=" form-control-label">Unit Allocation</label>
                                          <div class="input-group">
                                              <div class="input-group-addon">
                                                  <i class="fa fa-share-alt"></i>
                                              </div>
                                              {{ Form::select('unit_id', $units, null, ['class' => 'form-control','placeholder'=>'Please select']) }}
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <p><span>Vitals</span></p>
                        <hr />

                        <div class="row form-group">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="weight" class=" form-control-label">Weight</label>
                                    <div class="input-group">
                                        <input type="text" name="weight" id="weight" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="height" class=" form-control-label">Height</label>
                                    <div class="input-group">
                                        <input type="text" name="height" id="height" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="blood_pressure" class=" form-control-label">Blood Pressure</label>
                                    <div class="input-group">
                                        <input type="text" name="blood_presure" id="blood_pressure" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="pulse-rate" class=" form-control-label">Pulse Rate</label>
                                    <div class="input-group">
                                        <input type="text" name="pulse_rate" id="pulse_rate" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="respiratory-rate" class=" form-control-label">Respiratory Rate</label>
                                    <div class="input-group">
                                        <input type="text" name="respiratory_rate" id="respiratory_rate" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="temprature" class=" form-control-label">Temperature</label>
                                    <div class="input-group">
                                        <input type="text" name="temperature" id="temperature" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="other" class=" form-control-label">Other</label>
                                    <div class="input-group">
                                        <input type="text" name="other" id="other" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="nurse" class=" form-control-label">Name of Nurse</label>
                                    <div class="input-group">
                                        <input type="text" name="nurse" id="nurse" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary btn-sm">
                                      <i class="fa fa-dot-circle-o"></i> Submit
                                  </button>
                                  <button type="reset" class="btn btn-danger btn-sm">
                                      <i class="fa fa-ban"></i> Reset
                                  </button>
                              </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>