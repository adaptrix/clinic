<!-- modal take photo -->
<div class="modal fade" id="takephoto" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Capture Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="my_photo_booth">
                        <div id="my_camera" style="margin: 0 auto;"></div>
                        
                        <!-- Configure a few settings and attach camera -->
                        <script language="JavaScript">
                            Webcam.set({
                                // live preview size
                                width: 640,
                                height: 480,
                                
                                // device capture size
                                dest_width: 640,
                                dest_height: 480,
                                
                                // final cropped size
                                crop_width: 480,
                                crop_height: 480,
                                
                                // format and quality
                                image_format: 'jpeg',
                                jpeg_quality: 100,
                                
                                // flip horizontal (mirror mode)
                                flip_horiz: true
                            });
                            Webcam.attach( '#my_camera' );
                        </script> 
                        
                    </div>
                    
                    {{-- <div id="results" style="display:none">
                        <!-- Your captured image will appear here... -->
                    </div> --}}
                </div>
                <div class="modal-footer">
                    <!-- A button for taking snaps -->
                    <form>
                        <div id="pre_take_buttons" style="">
                            <!-- This button is shown before the user takes a snapshot -->
                            <input type=button class="btn btn-primary" value="Take Snapshot" onClick="preview_snapshot()">
                        </div>
                        <div id="post_take_buttons" style="display:none;">
                            <!-- These buttons are shown after a snapshot is taken -->
                            <input type=button class="btn btn-primary" value="&lt; Take Another" onClick="cancel_preview()">
                            <input type=button class="btn btn-success" value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
                        </div>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        
    </div>
    <script language="JavaScript">
        // preload shutter audio clip
        var shutter = new Audio();
        shutter.autoplay = false;
        shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
        
        function preview_snapshot() {
            // play sound effect
            try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
            shutter.play();
            
            // freeze camera so user can preview current frame
            Webcam.freeze();
            
            // swap button sets
            document.getElementById('pre_take_buttons').style.display = 'none';
            document.getElementById('post_take_buttons').style.display = '';
        }
        
        function cancel_preview() {
            // cancel preview freeze and return to live camera view
            Webcam.unfreeze();
            
            // swap buttons back to first set
            document.getElementById('pre_take_buttons').style.display = '';
            document.getElementById('post_take_buttons').style.display = 'none';
        }
        
        function save_photo() {
            // actually snap photo (from preview freeze) and display it
            Webcam.snap( function(data_uri) {
                // display results in page
                document.getElementById('child-img-display').innerHTML = 
                    '<img src="'+data_uri+'" alt="" width="150" height="150">';
                
                document.getElementById('child-photo').value = data_uri;
                // shut down camera, stop capturing
                
                //$('#takephoto').modal('toggle');
                JQuery('#takephoto').modal('hide');
    
                Webcam.reset();
                
            } );
        }
    </script>
    <!-- end take photo -->
    
    
    