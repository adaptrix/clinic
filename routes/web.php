<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('checkSession','HomeController@checkSession');

Route::middleware('auth')->group(function()
{
    Route::get('/',function()
    {
        if(Auth::user()->hasRole('admin'))
        {        
            return redirect('/admin');
        }
        if(Auth::user()->hasRole('doctor'))
        {        
            return redirect('/doctor');
        }
        if(Auth::user()->hasRole('pharmacist'))
        {        
            return redirect('/pharmacy');
        }
        if(Auth::user()->hasRole('laboratorist'))
        {        
            return redirect('/laboratory');
        }
        if(Auth::user()->hasRole('receptionist'))
        {        
            return redirect('/reception');
        }
    });

    Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
    {
        Route::get('/admin','AdminController@index');
        Route::get('/admin/patients/list',function(){
            $title = 'Ádmininstrator :: Patients';
            return view('admin.patient',compact('title'));
        });
        Route::get('/admin/appointment/list',function(){
            $title = 'Ádmininstrator :: Appointments';
            return view('admin.appointment',compact('title'));
        });
        Route::get('/admin/appointment/upcoming',function(){
            $title = 'Ádmininstrator :: Upcoming Appoinment';
            return view('admin.upcoming',compact('title'));
        });
        Route::get('/admin/medicine/list',function(){
            $title = 'Ádmininstrator :: Medicine';
            return view('admin.medicine',compact('title'));
        });
        Route::get('/admin/billing/list',function(){
            $title = 'Ádmininstrator :: Billings';
            return view('admin.billing',compact('title'));
        });
        Route::get('/admin/services',function(){
            $title = 'Ádmininstrator :: Services';
            return view('admin.service',compact('title'));
        });
        Route::get('/admin/servicemodes',function(){
            $title = 'Ádmininstrator :: Service Modes';
            return view('admin.servicemode',compact('title'));
        });
        Route::get('/admin/users',function(){
            $title = 'Ádmininstrator :: Users';
            return view('admin.users',compact('title'));
        });
        Route::get('/admin/settings',function(){
            $title = 'Ádmininstrator :: Settings';
            return view('admin.settings',compact('title'));
        });
        Route::get('/admin/rooms',function(){
            $title = 'Ádmininstrator :: Rooms';
            return view('admin.room',compact('title'));
        });
        Route::get('/admin/units',function(){
            $title = 'Ádmininstrator :: Unit Allocations';
            return view('admin.units',compact('title'));
        });

        Route::post('/admin/user/create','AdminController@addUser');
        Route::post('/admin/user/update','AdminController@updateUser');
        Route::post('/admin/room/create','AdminController@addRoom');
        Route::post('/admin/room/update','AdminController@updateRoom');
        Route::post('/admin/service/create','AdminController@addService');
        Route::post('/admin/service/update','AdminController@updateService');
        Route::post('/admin/servicemode/create','AdminController@addServiceMode');
        Route::post('/admin/servicemode/update','AdminController@updateServiceMode');
        Route::post('/admin/unit/create','AdminController@addUnit');
        Route::post('/admin/unit/update','AdminController@updateUnit');

    });

    Route::group(['middleware' => 'App\Http\Middleware\DoctorMiddleware'], function()
    {
        Route::get('/doctor', 'DoctorController@index');
        Route::get('/doctor/appointments','AppointmentController@index');
        Route::get('/doctor/patients', 'PatientsController@index');
        Route::get('/doctor/patient/view/{reg_no}', 'PatientsController@view');
        Route::get('/doctor/patient/history/{reg_no}', 'PatientsController@history');
        Route::get('/doctor/patient/history/view/{reg_no}/{id}', 'PatientsController@history_view');

    });

    Route::group(['middleware' => 'App\Http\Middleware\PharmacyMiddleware'], function()
    {
        Route::get('/pharmacy','PharmacyController@index');
        //billings
        Route::get('/pharmacy/billings', 'BillingController@pharmacyBilling');
        Route::get('/pharmacy/prescription','BillingController@prescriptionBilling');

    });

    Route::group(['middleware' => 'App\Http\Middleware\LabMiddleware'], function()
    {
        Route::get('/laboratory','LaboratoryController@index');

    });

    Route::group(['middleware' => 'App\Http\Middleware\ReceptionMiddleware'], function()
    {
        Route::get('/reception', 'PatientsController@index'); 
        Route::get('/reception/patients','PatientsController@index');
        Route::get('/reception/patient/edit/{reg_no}', 'PatientsController@edit');
        Route::get('/reception/patient/view/{reg_no}', 'PatientsController@view');

        Route::get('/reception/appointments','AppointmentController@index');        
        Route::get('/reception/appointment/edit/{id}', 'AppointmentController@edit');

        Route::get('/reception/billing', 'BillingController@index');       

    });


    Route::get('/photo','PatientsController@photo');
    
    Route::resource('patients', 'PatientsController');

    Route::get('/patient/visit','PatientsController@visit');
    Route::get('/patient-que/delete/{id}','PatientsController@delete_que');

    Route::get('/patientQue','DatatableController@patient_que');
    Route::get('/patientDocQue','DatatableController@doc_patient_que');
    Route::get('/patientHistory/{id}','DatatableController@patient_history');
    Route::get('/patientList','DatatableController@patient_list');
    Route::get('/patientPayment/{id}','DatatableController@patient_payment');

    Route::resource('appointments', 'AppointmentController');
    Route::get('/appointment/edit/{id}','AppointmentController@edit');

    Route::get('/appointmentUpcoming','DatatableController@upcoming_appointment');
    Route::get('/appointmentList','DatatableController@appointment_list');
    Route::get('/appointmentDoc','DatatableController@doc_appointment');

    Route::resource('billings','BillingController');
    Route::get('/billing/list','DatatableController@billingList');
    Route::get('/billing/medicine','DatatableController@billingMedicine');
    Route::get('/billing/lab','DatatableController@billingLab');
    Route::get('/billing/patient','DatatableController@billingPatient');
    Route::get('/billing/reception','DatatableController@billingReception');

    Route::get('/autocomplete/{id}','Patients@autocomplete_ajax');
    Route::get('/name-ajax','PatientsController@name_ajax');
    Route::get('/getRegNo','PatientsController@generatePatientRegNo');
    Route::get('/getAge','PatientsController@getAge');

    Route::get('/getCharges/{id}','AppointmentController@getCharges');

    Route::get('/users','DatatableController@users');
    Route::get('/rooms','DatatableController@rooms');
    Route::get('/services','DatatableController@services');
    Route::get('/servicemodes','DatatableController@servicemodes');
    Route::get('/units','DatatableController@units');

});