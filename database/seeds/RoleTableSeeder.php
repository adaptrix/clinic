<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'An Administrator';
        $role_admin->save();

        $role_doctor = new Role();
        $role_doctor->name = 'doctor';
        $role_doctor->description = 'A doctor';
        $role_doctor->save();

        $role_pharmacist = new Role();
        $role_pharmacist->name = 'pharmacist';
        $role_pharmacist->description = 'A Pharmacy User';
        $role_pharmacist->save();

        $role_laboratorist = new Role();
        $role_laboratorist->name = 'laboratorist';
        $role_laboratorist->description = 'A laboratory User';
        $role_laboratorist->save();

        $role_receptionist = new Role();
        $role_receptionist->name = 'receptionist';
        $role_receptionist->description = 'A Reception User';
        $role_receptionist->save();        
    }
}
