<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::whereName('admin')->first();
        $role_doctor = Role::whereName('doctor')->first();
        $role_pharmacist = Role::whereName('pharmacist')->first();
        $role_laboratorist = Role::whereName('laboratorist')->first();
        $role_receptionist = Role::whereName('receptionist')->first();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@clinic.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $doctor = new User();
        $doctor->name = 'Doctor';
        $doctor->email = 'doctor@clinic.com';
        $doctor->password = bcrypt('doctor');
        $doctor->save();
        $doctor->roles()->attach($role_doctor);

        $pharmacy = new User();
        $pharmacy->name = 'Pharmacist';
        $pharmacy->email = 'pharmacist@clinic.com';
        $pharmacy->password = bcrypt('pharmacy');
        $pharmacy->save();
        $pharmacy->roles()->attach($role_pharmacist);

        $lab = new User();
        $lab->name = 'Laboratorist';
        $lab->email = 'lab@clinic.com';
        $lab->password = bcrypt('laboratory');
        $lab->save();
        $lab->roles()->attach($role_laboratorist);

        $reception = new User();
        $reception->name = 'Receptionist';
        $reception->email = 'receptionist@clinic.com';
        $reception->password = bcrypt('reception');
        $reception->save();
        $reception->roles()->attach($role_receptionist);
    }
}
