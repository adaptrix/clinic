<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table)
        {
            $table->increments('medicine_id');
            $table->string('name');
            $table->string('medicine_category_id');
            $table->mediumText('description');
            $table->decimal('buying_price');
            $table->decimal('selling_price');
            $table->string('supplier');
            $table->string('quantity');
            $table->string('units');
            $table->date('expiry_date');
            $table->date('invoice_date');
            $table->string('invoice_no');
            $table->string('batch_no');
            $table->timestamps();
            
            $table->foreign('medicine_category_id')->references('medicine_category_id')->on('medicine_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
