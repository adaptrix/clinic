<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table)
        {
            $table->increments('appointment_id');
            $table->string('doc_id');
            $table->datetime('appointment_date');
            $table->string('patient_reg_no')->nullable();
            $table->string('service_id');
            $table->string('name');
            $table->string('room_no');
            $table->string('billing_id');
            $table->boolean('paid')->default(0);
            $table->string('status');
            $table->string('checkin');
            $table->datetime('report_time')->nullable();
            $table->timestamps();

            $table->foreign('doc_id')->references('doc_id')->on('doctors');
            $table->foreign('patient_reg_no')->references('patient_reg_no')->on('patients');
            $table->foreign('service_id')->references('service_id')->on('services');
            $table->foreign('billing_id')->references('billing_id')->on('billings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
