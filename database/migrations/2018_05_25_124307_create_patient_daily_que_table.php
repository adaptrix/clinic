<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDailyQueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_daily_que', function (Blueprint $table) 
        {
            $table->increments('patient_que_id');
            $table->string('patient_reg_no');
            $table->string('service_mode_id');
            $table->string('service_id');
            $table->string('doc_id');
            $table->string('unit_id');
            $table->string('room_no');
            $table->date('date');
            $table->timestamps();

            $table->foreign('patient_reg_no')->references('patient_reg_no')->on('patients');
            $table->foreign('service_mode_id')->references('service_mode-id')->on('service_modes');
            $table->foreign('service_id')->references('service_id')->on('services');
            $table->foreign('doc_id')->references('doc_id')->on('doctors');
            $table->foreign('unit_id')->references('unit_id')->on('unit_allocation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_daily_que');
    }
}
