<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) 
        {
            $table->increments('billing_id');
            $table->string('service_id');
            $table->string('doc_id');
            $table->string('unit_id');
            $table->string('patient_reg_no');
            $table->string('payment_mode_id');
            $table->date('service_date');
            $table->decimal('total_amount');
            $table->decimal('amount_paid');
            $table->decimal('amount_due');
            $table->decimal('discount');
            $table->timestamps();

            $table->foreign('service_id')->references('service_id')->on('services');
            $table->foreign('doc_id')->references('doc_id')->on('doctors');
            $table->foreign('unit_id')->references('unit_id')->on('unit_allocation');
            $table->foreign('patient_reg_no')->references('reg_no')->on('patients');
            $table->foreign('payment_mode_id')->references('payment_mode_id')->on('payment_modes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
