<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_reg_no');
            $table->string('weight');
            $table->string('height');
            $table->string('blood_pressure');
            $table->string('pulse_rate');
            $table->string('respiratory_rate');
            $table->string('temperature');
            $table->string('other')->nullable();
            $table->string('nurse');
            $table->timestamps();

            $table->foreign('patient_reg_no')->references('reg_no')->on('patients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitals');
    }
}
