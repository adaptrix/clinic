
patientRegNo = function()
{
    $.ajax({
        url: '/clinic/getRegNo',
        type: 'GET',
        dataType: 'json',
        data: ''
    })
    .done(function(data){
        $('#adult_reg_no').val(data.reg_no);
        $('#child_reg_no').val(data.reg_no);
    })
    .fail(function(data){
        console.log(data);
    });
}

patientRegNo();

$('#dob').on('change',function(){
    var dob_val = $('#dob').val();
    $.ajax({
        url: '/clinic/getAge',
        type: 'GET',
        dataType: 'json',
        data: {dob:dob_val}
    })
    .done(function(data){
        $('#age-year').val(data.years);
        $('#age-month').val(data.months);
        $('#age-day').val(data.days);
    })
    .fail(function(data){
        console.log(data);
    });
});

$('#adult-dob').on('change',function(){
    var dob_val = $('#adult-dob').val();
    $.ajax({
        url: '/clinic/getAge',
        type: 'GET',
        dataType: 'json',
        data: {dob:dob_val}
    })
    .done(function(data){
        $('#adult-year').val(data.years);
        $('#adult-month').val(data.months);
    })
    .fail(function(data){
        console.log(data);
    });
});

$('#age-day').on('change',function(){
    var year = $('#age-year').val();
    var month = $('#age-month').val();
    var day = $('#age-day').val();
    
    var birthDate = moment().subtract(year, 'years').subtract(month, 'months').subtract(day, 'days');
    $('#dob').val(birthDate.format("YYYY-MM-DD"));
});

$('#adult-month').on('change',function(){
    var year = $('#adult-year').val();    
    var month = $('#adult-month').val();    
    var birthDate = moment().subtract(year, 'years').subtract(month, 'months');
    $('#adult-dob').val(birthDate.format("YYYY-MM-DD"));
});

$('#capturePhoto').on('click',function(e){
    e.preventDefault();
    var type = 'adult';
    $.ajax({
        url: '/clinic/photo',
        type: 'GET',
        dataType: 'html',
        data: {type:type}
    })
    .done(function(feedback) {
        $('.child-captured_image').html('');
        $('.captured_image').html(feedback);
        $('#takephoto').modal('show');
    })
    .fail(function(resp) {
        console.log(resp);
    });
});

$('#child-capturePhoto').on('click',function(e){
    e.preventDefault();
    var type = 'child';
    $.ajax({
        url: '/clinic/photo',
        type: 'GET',
        dataType: 'html',
        data: {type:type}
    })
    .done(function(feedback) {
        $('.captured_image').html('');
        $('.child-captured_image').html(feedback);
        $('#takephoto').modal('show');
    })
    .fail(function(resp) {
        console.log(resp);
    });
});

$('#check-name').autocomplete({          
    source: '/clinic/name-ajax',
    highlight: true,
    minLength: 2,
    select: function( event, ui ) {
        $('#check-id').val(ui.item.id);
        $('#check-name').val(ui.item.value);
    }
});

$('#check-name').on('change',function(){
    var id = $('#check-id').val();
    $.ajax({					
        type:'get',					
        url:'/clinic/autocomplete' + '/' + id,	
        dataType: 'json',	
        data:'',		
    })
    .done(function(patient) {
        $('.reg_no').val(patient.reg_no);
        $('.que_no').val(patient.que_no);
        $('.type').val(patient.p_type);
        $('.email').val(patient.email);
        $('.phone').val(patient.phone);
        $('.address').val(patient.address);
        $('.gender').val(patient.gender);
        $('#p-photo').attr('src','assests/images/patient/' + patient.photo);
    })
    .fail(function() {
        console.log("error");
    });
});

var patient_form = $('#register-new-patient');
patient_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        patient_form.trigger('reset');
        Lobibox.notify('success',{
            msg: 'New patient regisration was successfull'
        });

        patientRegNo();
    })
    .fail(function(error) {
      console.log("error:"+error);
    });
});

var visit_form = $('#new-patient-visit');
visit_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        url: 'patient/visit',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        visit_form.trigger('reset');
        Lobibox.notify('success',{
            msg: 'The patient as been added to the queue'
        });

        patientRegNo();
    })
    .fail(function() {
      console.log("error");
    });
});

var appointment_form = $('#new-appointment');
appointment_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        appointment_form.trigger('reset');
        Lobibox.notify('success',{
            msg: 'Appointment created successfully'
        });
    })
    .fail(function() {
      console.log("error");
    });
});

var bill_create_form = $('#billing-create');
bill_create_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        bill_create_form.trigger('reset');
        Lobibox.notify('success',{
            msg: 'Payment made successfully'
        });
    })
    .fail(function() {
      console.log("error");
    });
});

var bill_update_form = $('#billing-update');
bill_update_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        bill_update_form.trigger('reset');
        Lobibox.notify('success',{
            msg: 'Payment updated successfully'
        });
    })
    .fail(function() {
      console.log("error");
    });
});

var patient_que_table = $('#patient-que-table');
patient_que_table.on('draw', function(event) {
    patient_que_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        $(this.node()).find('.delete').on('click', function(event) {
            event.preventDefault(); 
            var link = $(this);                  
            var id = link.data('id');

            $.ajax({
                url: 'patient-que/delete/' + id,
                type: 'POST',
                dataType: 'json',
                data: {id: id},
            })
            .done(function() {
                que_table.draw(false);
            })
            .fail(function() {
                console.log("error");
            });
            
        });
    });
}); 

var patient_update_form = $('#update-patient');
patient_update_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        patient_update_form.trigger('reset');
        Lobibox.notify('success',{
            msg: 'Patient details updated successfully'                    
        });

        window.location = 'patients';
    })
    .fail(function() {
        console.log("error");
    });
});


