$('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
} );

var patient_history_table = $('#patient-history');
var p_reg_no = $('#p-id').val();
patient_history_table = patient_history_table.DataTable({  
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: 'patientHistory/' + p_reg_no,
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'date', name: 'date'},
        {data: 'service', name: 'service'}, 
        {data: 'doctor', name: 'doctor'}, 
        {data: 'exam', name: 'exam'}, 
        {data: 'diagnoses', name: 'diagnoses'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});


var patient_list_table = $('#patient-list-table');
patient_list_table = patient_list_table.DataTable({   
    scrollX: true,             
    processing: true,
    serverSide: true,
    ajax: '/clinic/patientList',
    pageLength:25,
    ordering:true,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'reg_no', name: 'reg_no'},
        {data: 'name', name: 'name'}, 
        {data: 'phone', name: 'phone'}, 
        {data: 'age', name: 'age'}, 
        {data: 'type', name: 'type'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

var patient_que_table = $('#patient-que-table');
patient_que_table = patient_que_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/patientQue',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'que_no', name: 'que_no'}, 
        {data: 'date', name: 'date'},
        {data: 'reg_no', name: 'reg_no'},
        {data: 'name', name: 'name'}, 
        {data: 'type', name: 'type'}, 
        {data: 'service_mode', name: 'service_mode'},
        {data: 'service', name: 'service'}, 
        {data: 'doctor', name: 'doctor'}, 
        {data: 'unit', name: 'unit'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    patient_que_table.ajax.reload( null, false );
}, 30000 );

var doc_que_table = $('#patient-doc-que');
doc_que_table = doc_que_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/patientDocQue',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'date', name: 'date'},
        {data: 'reg_time', name: 'reg_time'},
        {data: 'que_no', name: 'que_no'},
        {data: 'name', name: 'name'}, 
        {data: 'type', name: 'type'}, 
        {data: 'service_mode', name: 'service_mode'},
        {data: 'waiting_time', name: 'waiting_time'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    doc_que_table.ajax.reload( null, false );
}, 30000 );

var patient_payment_table = $('#patient-payment');
patient_payment_table = patient_payment_table.DataTable({   
    scrollX: true,             
    processing: true,
    serverSide: true,
    ajax: "/clinic/patientPayment/" + p_reg_no,
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'date', name: 'date'},
        {data: 'service', name: 'service'}, 
        {data: 'doctor', name: 'doctor'}, 
        {data: 'payment_mode', name: 'payment_mode'},
        {data: 'amount_paid', name: 'amount_paid'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'discount', name: 'discount'}
    ]
});

var appointment_table = $('#appointment-que');        
appointment_table = appointment_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/appointmentList',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'que_no', name: 'que_no'}, 
        {data: 'date', name: 'date'},
        {data: 'name', name: 'name'},
        {data: 'doctor', name: 'doctor'}, 
        {data: 'room', name: 'room'},
        {data: 'charge', name: 'charge'},
        {data: 'advance_pay', name: 'advance_pay'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    appointment_table.ajax.reload( null, false ); // user paging is not reset on reload
}, 30000 );

var upcoming_appoint_table = $('#upcoming-que');
upcoming_appoint_table = upcoming_appoint_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/appointmentUpcoming',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'que_no', name: 'que_no'}, 
        {data: 'date', name: 'date'},
        {data: 'name', name: 'name'},
        {data: 'doctor', name: 'doctor'}, 
        {data: 'room', name: 'room'},
        {data: 'checkin', name: 'checkin'},
        {data: 'charge', name: 'charge'},
        {data: 'advance_pay', name: 'advance_pay'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    upcoming_appoint_table.ajax.reload( null, false ); 
}, 30000 );

var billing_list_table = $('#billing-list');
billing_list_table = billing_list_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/billing/list',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'trans_id', name: 'trans_id'}, 
        {data: 'date', name: 'date'},
        {data: 'reg_no', name: 'reg_no'},
        {data: 'name', name: 'name'}, 
        {data: 'service', name: 'service'},
        {data: 'doctor', name: 'doctor'},
        {data: 'amount', name: 'amount'},
        {data: 'amount_paid', name: 'amount_paid'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'discount', name: 'discount'}
    ]
});

setInterval( function () {
    billing_list_table.ajax.reload( null, false ); 
}, 30000 );

var medicine_billing_table = $('#medicine-billing');
medicine_billing_table = medicine_billing_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/billing/medicine',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'date', name: 'date'}, 
        {data: 'name', name: 'name'},
        {data: 'medicine', name: 'medicine'},
        {data: 'doctor', name: 'doctor'}, 
        {data: 'amount', name: 'amount'},
        {data: 'amount_paid', name: 'amount_paid'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'discount', name: 'discount'},
        {data: 'time', name: 'time'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    medicine_billing_table.ajax.reload( null, false ); 
}, 30000 );

var lab_billing_table = $('#lab-billing');
lab_billing_table = lab_billing_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/billing/lab',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'date', name: 'date'}, 
        {data: 'reg_no', name: 'reg_no'}, 
        {data: 'name', name: 'name'},
        {data: 'service', name: 'service'},
        {data: 'doctor', name: 'doctor'}, 
        {data: 'amount', name: 'amount'},
        {data: 'amount_paid', name: 'amount_paid'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'discount', name: 'discount'},
        {data: 'time', name: 'time'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    lab_billing_table.ajax.reload( null, false ); 
}, 30000 );

var patient_billing_table = $('#patient-billing');
patient_billing_table = patient_billing_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/billing/patient',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'date', name: 'date'}, 
        {data: 'reg_no', name: 'reg_no'}, 
        {data: 'name', name: 'name'},
        {data: 'service', name: 'service'},
        {data: 'doctor', name: 'doctor'}, 
        {data: 'amount', name: 'amount'},
        {data: 'amount_paid', name: 'amount_paid'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'discount', name: 'discount'},
        {data: 'time', name: 'time'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    patient_billing_table.ajax.reload( null, false ); 
}, 30000 );

var reception_billing_table = $('#billing-reception');
reception_billing_table = reception_billing_table.DataTable({   
    scrollX: true,             
    processing: false,
    serverSide: true,
    ajax: '/clinic/billing/reception',
    pageLength:25,
    ordering:false,
    bAutoWidth: false,
    oSearch: {"sSearch": ''},
    columns: [
        {data: 'trans_id', name: 'trans_id'}, 
        {data: 'date', name: 'date'},
        {data: 'reg_no', name: 'reg_no'},
        {data: 'name', name: 'name'}, 
        {data: 'service', name: 'service'},
        {data: 'doctor', name: 'doctor'},
        {data: 'amount', name: 'amount'},
        {data: 'amount_paid', name: 'amount_paid'},
        {data: 'amount_due', name: 'amount_due'},
        {data: 'discount', name: 'discount'},
        {data: 'actions', name: 'actions',orderable: false, searchable: false}
    ]
});

setInterval( function () {
    reception_billing_table.ajax.reload( null, false ); 
}, 30000 );
