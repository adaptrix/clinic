<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $date = ['DOB','created_at'];
    protected $table = 'patients';

    // public function patientRegNo()
	// {
	// 	return 'P-'.sprintf("%04d", $this->id);
	// }
}
