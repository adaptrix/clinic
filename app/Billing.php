<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $date = ['created_at'];
    protected $primaryKey = 'billing_id';
}
