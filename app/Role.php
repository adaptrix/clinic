<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function permissions(){
		return $this->belongsToMany('Permission');
	}
	
	public function hasPermission($permission){
		$permissions = $this->permissions()->lists('id');

		if(in_array($permission, $permissions)){
			return true;
		}
		return false;
	}
}
