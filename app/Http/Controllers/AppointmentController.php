<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Appointment;
use App\Service;
use App\Room;
use App\Doctor;

use Auth;

class AppointmentController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if($user->hasRole('doctor'))
        {
            $title = 'Doctor :: Appointments';
            $rooms = Room::pluck('name','id');
            $services = Service::pluck('service_name','service_id');
            $doctors = Doctor::pluck('name','doc_id');
            $appointments = Appointment::where('doc_id',$user->employee_id)->get();
            $upcoming = Appointment::where('doc_id',$user->employee_id)->where('appointment_date',date('d-m-Y'))->get();

        }
        if(Auth::user()->hasRole('receptionist'))
        {
            $title = 'Reception :: Appointments';
            $rooms = Room::pluck('name','id');
            $services = Service::pluck('service_name','service_id');
            $doctors = Doctor::pluck('name','doc_id');
            $appointments = Appointment::all();
            $upcoming = Appointment::where('appointment_date',date('d-m-Y'))->get();
        }       
        
        return view('appointments.index',compact('title','appointments','upcoming','rooms','services','doctors'));
    }

    public function store(Request $request)
    {
        Appointment::create($request->all());
    }
    
    public function edit($id)
    {
        Appointment::findOrFail($id);
    }

    public function update($id, Request $request)
    {
        $appointment = Appointment::find($id);
        $appointment->update($request->all());
    }

    public function delete($id)
    {
        Appointment::destroy($id);
    }

    public function status($id, Request $request)
    {
        $appointment = Appointment::find($id);
        $appointment->update(['status' => $request->status]);
    }
    
    public function checkIn($id)
    {
        $appointment = Appointment::find($id);
        $appointment->update(['checkin' => 'yes','report_time' => date('d-m-Y h:m:s')]);
    }

    public function getCharges($id)
    {
        $service = Service::findOrFail($id);
        return response()->json(['amount' => $service->price]);
    }
}
