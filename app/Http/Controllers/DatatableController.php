<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Datatables;
use Auth;
use App\Patient;
use App\PatientQueue;
use App\Billing;
use App\Diagnosis;
use App\Appointment;
use App\Prescription;
use App\User;
use App\Room;
use App\Service;
use App\ServiceMode;
use App\UnitAllocation;

class DatatableController extends Controller
{
    //patients
    public function patient_que()
    {
        $queue = PatientQueue::orderBy('date','desc')
            ->leftJoin('patients', 'patients.reg_no', '=', 'patient_daily_que.patient_reg_no')
            ->leftJoin('doctors','doctors.doc_id','=','patient_daily_que.doc_id')
            ->leftJoin('services','services.service_id','=','patient_daily_que.service_id')
            ->leftJoin('unit_allocation','unit_allocation.unit_id','=','patient_daily_que.unit_id')
            ->leftJoin('service_modes','service_modes.service_mode_id','=','patient_daily_que.service_mode_id')
            ->select('patient_daily_que.patient_que_id as que_no','patient_daily_que.date as date','patients.reg_no as reg_no','patients.name as name','patients.type as type','services.service_name as service','service_modes.name as service_mode','unit_allocation.unit_name as unit','doctors.name as doctor');
        
        $table = Datatables::of($queue)
            ->editColumn('actions',function($data)
            {
                $output = '<div class="table-data-feature"><button class="btn btn-danger" data-id="'.$data->que_no.'"><i class="zmdi zmdi-delete"> Delete</i></button></div>';
                return $output;
            })->make(true);  
            
        return $table;
                                              
    }

    public function doc_patient_que()
    {        
        $doc_queue = PatientQueue::orderBy('date','desc')
            ->where('doc_id',Auth::user()->employee_id)
            ->leftJoin('patients', 'patients.reg_no', '=', 'patient_daily_que.patient_reg_no')
            ->leftJoin('service_modes', 'service_modes.service_mode_id', '=', 'patient_daily_que.service_mode_id')
            ->select('patient_daily_que.patient_que_id as que_no','patient_daily_que.date as date','patients.name as name','patients.type as type','service_modes.name as service_mode','patient_daily_que.date as reg_time','patient_daily_que.created_at as waiting_time','patients.reg_no as reg_no');
        
        $table = Datatables::of($doc_queue)
            ->editColumn('actions',function($data)
            {
                $output = '<div class="table-data-feature"><button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Serve" data-id="'.$data->reg_no.'"><i class="fa fa-user-md"></i> Serve</button></div>';

                return $output;
            })->make(true); 
        return $table;
    }

    public function patient_list()
    {
        $user = Auth::user();
        $patient_list = Patient::orderBy('created_at','desc')
            ->select('reg_no','name','type','phone','DOB as age');

        $table = Datatables::of($patient_list)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature">';
            if($user->hasRole('admin') || $user->hasRole('doctor'))
            {
                $output .= '<a href="'.url('/doctor/patient/history').'/'.$data->reg_no.'" class="" data-toggle="tooltip" data-placement="top" title="History"><button class="btn btn-primary"><i class="zmdi zmdi-folder"></i> History</button>';
            }
            if($user->hasRole('admin') || $user->hasRole('receptionist'))
            {
                $output .= '<a href="'.url('/reception/patient/edit').'/'.$data->reg_no.'" class="" data-toggle="tooltip" data-placement="top" title="Edit"><button class="btn btn-success"><i class="zmdi zmdi-edit"></i> Edit</button>
                </a>';
            }

            $output .= '<a href="'.$user->hasRole('doctor')? url('/doctor/patient/view').'/'.$data->reg_no : url('/reception/patient/view').'/'.$data->reg_no.'" class="" data-toggle="tooltip" data-placement="top" title="View"><button class="btn btn-primary"><i class="zmdi zmdi-eye"></i> View</button>';

            if($user->hasRole('admin'))
            {
                $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'"><i class="zmdi zmdi-delete"></i> Delete</button>';
            }
            $output .= '</div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function patient_history($reg_no)
    {
        $history = Diagnosis::orderBy('created_at','desc')
            ->where('patient_reg_no',$reg_no)
            ->leftJoin('services','services.service_id','=','diagnosis.service_id')
            ->select('diagnosis.date as date','diagnosis.examination as exam','diagnosis.diff_diagnosis as diagnoses','services.service_name as service','diagnosis.diagnosis_id as id','diagnosis.patient_reg_no as reg_no');

        $table = Datatables::of($history)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><a href="'.url('/doctor/patient/history/view').'/'.$data->reg_no.'/'.$data->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View"><i class="zmdi zmdi-eye"></i> View</a></div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function patient_payment($reg_no)
    {
        $payment = Billing::orderBy('created_at','desc')
            ->where('patient_reg_no',$reg_no)
            ->leftJoin('services','services.service_id','=','billings.service_id')
            ->leftJoin('doctors','doctors.doc_id','=','billings.doc_id')
            ->leftJoin('service_modes','payment_modes.payment_mode_id','=','billings.payment_mode_id')
            ->select('billings.created_at as date','services.service_name as service','doctor.name as doctor','payment_modes.name as payment_mode','billings.amount_paid as amount_paid','billings.amount_due as amount_due');

        $table = Datatables::of($doc_queue)->make(true); 

        return $table;
    }

    //appointments
    public function upcoming_appointment()
    {
        $user = Auth::user();
        if($user->hasRole('doctor'))
        {
            $upcoming = Appointment::orderBy('appointment_date','desc')            
            ->leftJoin('doctors','doctors.id','=','appointments.doc_id')
            ->leftJoin('billings','billings.billing_id','=','appointments.billing_id')
            ->where('appointments.doc_id',$user->employee_id)
            ->select('appointments.patient_reg_no as reg_no','appointments.name as name','appointments.appointment_id as que_no','appointments.room_no as room','appointments.appointment_date as date','appointments.checkin as checkin','doctors.name as doctor','billings.amount_due as amount_due','billings.amount_paid as advance_pay','billings.total_amount as charge');
        }
        else
        {
            $upcoming = Appointment::orderBy('appointment_date','desc')
            ->leftJoin('doctors','doctors.id','=','appointments.doc_id')
            ->leftJoin('billings','billings.billing_id','=','appointments.billing_id')
            ->select('appointments.patient_reg_no as reg_no','appointments.name as name','appointments.appointment_id as que_no','appointments.room_no as room','appointments.appointment_date as date','doctors.name as doctor','billings.amount_due as amount_due','billings.amount_paid as advance_pay','billings.total_amount as charge');
        }
        

        $table = Datatables::of($upcoming)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature">';
            if($user->hasRole('receptionist'))
            {
                $output .= '<button id="checkin" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Check-in" data-id="'.$data->id.'"><i class="zmdi zmdi-check"></i> Check-in</button>';

                $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'"><i class="zmdi zmdi-delete"></i> Delete</button>';
            }
            if($user->hasRole('doctor'))
            {
                $output .= '<button id="approve" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Approve" data-id=".$data->id."><i class="zmdi zmdi-check"></i> Approve</button>';

                $output .= '<button id="reject" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Reject" data-id="'.$data->id.'"><i class="fa fa-ban"></i> Reject</button>';
            }
            $output .= '</div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function appointment_list()
    {
        $user = Auth::user();
        if($user->hasRole('doctor'))
        {
            $upcoming = Appointment::orderBy('appointment_date','desc')
            ->where('appointments.doc_id',$user->employee_id)
            ->leftJoin('doctors','doctors.id','=','appointments.doc_id')
            ->leftJoin('billings','billings.billing_id','=','appointments.billing_id')
            ->select('appointments.patient_reg_no as reg_no','appointments.name as name','appointments.appointment_id as que_no','appointments.room_no as room','appointments.appointment_date as date','doctors.name as doctor','billings.amount_due as amount_due','billings.amount_paid as advance_pay','billings.total_amount as charge');
        }
        else
        {
            $upcoming = Appointment::orderBy('appointment_date','desc')
            ->leftJoin('doctors','doctors.id','=','appointments.doc_id')
            ->leftJoin('billings','billings.billing_id','=','appointments.billing_id')
            ->select('appointments.patient_reg_no as reg_no','appointments.name as name','appointments.appointment_id as que_no','appointments.room_no as room','appointments.appointment_date as date','doctors.name as doctor','billings.amount_due as amount_due','billings.amount_paid as advance_pay','billings.total_amount as charge');
        }

        $table = Datatables::of($upcoming)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature">';
            if($user->hasRole('receptionist'))
            {
                //$output .= '<a href="#" data-target="#editAppointment" data-toggle="modal"><button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="zmdi zmdi-edit"></i> Edit</button></a>';                

                $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'"><i class="zmdi zmdi-delete"></i> Delete</button>';
            }
            // if($user->hasRole('doctor'))
            // {
            //     $output .= '<button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Approve" data-id=".$data->id."><i class="zmdi zmdi-check"></i> Approve</button>';

            //     $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Reject" data-id="'.$data->id.'"><i class="fa fa-ban"></i> Reject</button>';
            // }
            $output .= '</div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function billingList()
    {
        $billing = Billing::orderBy('billings.created_at','desc')
            ->leftJoin('services','services.service_id','=','billings.service_id')
            ->leftJoin('patients','patients.reg_no','=','billings.patient_reg_no')
            ->leftJoin('doctors','doctors.doc_id','=','billings.doc_id')
            ->select('billings.patient_reg_no as reg_no','billings.transaction_id as trans_id','billings.total_amount as amount', 'billings.amount_paid as amount_paid', 'billings.amount_due as amount_due','billings.discount as discount','billings.service_date as date','patients.name as name','doctors.name as doctor','services.service_name as service');

        $table = Datatables::of($billing)->make(true); 

        return $table;
    }

    public function billingMedicine()
    {
        $prescription = Billing::orderBy('billings.created_at','desc')
            ->leftJoin('services','services.service_id','=','billings.service_id')
            ->leftJoin('patients','patients.reg_no','=','billings.patient_reg_no')
            ->leftJoin('doctors','doctors.doc_id','=','billings.doc_id')
            ->select('billings.patient_reg_no as reg_no','billings.transaction_id as trans_id','billings.total_amount as amount', 'billings.amount_paid as amount_paid', 'billings.amount_due as amount_due','billings.discount as discount','billings.service_date as date','patients.name as name','doctors.name as doctor','services.service_name as service');

        $table = Datatables::of($prescription)
        ->editColumn('medicine',function($data)
        {
            $output = '<a href="#" data-target="#medList" data-id="'.$data->reg_no.'" data-toggle="modal"><button class="btn btn-primary"><i class="zmdi zmdi-eye"> View</i></button></a>';
            return $output;
        })
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><a href="#" data-target="#payBill" data-toggle="modal"><button class="btn btn-primary"><i class="zmdi zmdi-money">Pay</i></button></a>';

            $output .= '<button class="btn btn-danger"><i class="fa fa-ban"> Cancel</i></button></div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function billingLab()
    {
        $history = Diagnosis::orderBy('diagnosis.created_at','desc')
            ->leftJoin('services','services.service_id','=','diagnosis.service_id')
            ->select('diagnosis.date as date','diagnosis.examination as exam','diagnosis.diff_diagnosis as diagnoses','services.service_name as service','diagnosis.diagnosis_id as id','diagnosis.patient_reg_no as reg_no');

        $table = Datatables::of($history)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><a href="'.url('/doctor/patient/history/view').'/'.$data->reg_no.'/'.$data->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View"><i class="zmdi zmdi-eye"></i> View</a></div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function billingPatient()
    {
        $billing = Billing::orderBy('billings.created_at','desc')
            ->leftJoin('services','services.service_id','=','billings.service_id')
            ->leftJoin('patients','patients.reg_no','=','billings.patient_reg_no')
            ->leftJoin('doctors','doctors.doc_id','=','billings.doc_id')
            ->select('billings.patient_reg_no as reg_no','billings.transaction_id as trans_id','billings.total_amount as amount', 'billings.amount_paid as amount_paid', 'billings.amount_due as amount_due','billings.discount as discount','billings.service_date as date','patients.name as name','doctors.name as doctor','services.service_name as service');

        $table = Datatables::of($billing)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><a href="#" data-target="#payBill" data-toggle="modal"><button class="btn btn-primary"><i class="zmdi zmdi-money">Pay</i></button></a>';

            $output .= '<button class="btn btn-danger"><i class="fa fa-ban"> Cancel</i></button></div>';

            return $output;
        })->make(true); 

        return $table;
    }

    public function billingReception()
    {
        $billing = Billing::orderBy('billings.created_at','desc')
            ->leftJoin('services','services.service_id','=','billings.service_id')
            ->leftJoin('patients','patients.reg_no','=','billings.patient_reg_no')
            ->leftJoin('doctors','doctors.doc_id','=','billings.doc_id')
            ->select('billings.patient_reg_no as reg_no','billings.transaction_id as trans_id','billings.total_amount as amount', 'billings.amount_paid as amount_paid', 'billings.amount_due as amount_due','billings.discount as discount','billings.service_date as date','patients.name as name','doctors.name as doctor','services.service_name as service');

        $table = Datatables::of($billing)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><a href="#" data-target="#payBill" data-toggle="modal"><button class="btn btn-primary"><i class="zmdi zmdi-money">Pay</i></button></a>';

            $output .= '<button class="btn btn-danger"><i class="fa fa-ban"> Cancel</i></button></div>';

            return $output;
        })->make(true); 

        return $table;
    }    
    
    public function users()
    {
        $users = User::orderBy('users.name','desc')
        ->leftjoin('role_user','role_user.user_id','=','users.id')
        ->leftjoin('roles','roles.id','=','role_user.role_id')
        ->select('users.id as id','users.name as name','email','employee_id','roles.name as role');

        $table = Datatables::of($users)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" data-id="'.$data->id.'" onclick="user('.$data->id.')"><i class="zmdi zmdi-edit"></i> Edit</button>';

            $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'"><i class="zmdi zmdi-delete"></i> Delete</button></div>';

            return $output;
        })->make(true); 

        return $table;
    }
    public function rooms()
    {
        $rooms = Room::orderBy('created_at','desc')
        ->select('id','name as room_no','room_use as use');

        $table = Datatables::of($rooms)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" data-id="'.$data->id.'" onclick="room('.$data->id.')"><i class="zmdi zmdi-edit"></i> Edit</button>';

            $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'"><i class="zmdi zmdi-delete"></i> Delete</button></div>';

            return $output;
        })->make(true); 

        return $table;
    }
    public function services()
    {
        
        $services = Service::orderBy('created_at','desc')
        ->select('service_id as id','service_name as service','price');

        $table = Datatables::of($services)
        ->editColumn('actions',function($data)
        {
            $delete = "'delete'";
            $get = "'get'";

            $output = '<div class="table-data-feature"><button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" data-id="'.$data->id.'" onclick="service('.$data->id.','.$get.')"><i class="zmdi zmdi-edit"></i> Edit</button>';

            $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'" onclick="service('.$data->id.','.$delete.')"><i class="zmdi zmdi-delete"></i> Delete</button></div>';

            return $output;
        })->make(true); 

        return $table;
    }
    public function servicemodes()
    {
        $servicemodes = ServiceMode::orderBy('created_at','desc')
        ->select('service_mode_id as id','name as servicemode','price');

        $table = Datatables::of($servicemodes)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" data-id="'.$data->id.'" onclick="servicemode('.$data->id.')"><i class="zmdi zmdi-edit"></i> Edit</button>';

            $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'"><i class="zmdi zmdi-delete"></i> Delete</button></div>';

            return $output;
        })->make(true); 

        return $table;
    }
    public function units()
    {
        $units = UnitAllocation::orderBy('created_at','desc')
        ->select('unit_id as id','unit_name as unit');

        $table = Datatables::of($units)
        ->editColumn('actions',function($data)
        {
            $output = '<div class="table-data-feature"><button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" data-id="'.$data->id.'"><i class="zmdi zmdi-edit"></i> Edit</button>';

            $output .= '<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" data-id="'.$data->id.'" onclick="user('.$data->id.')"><i class="zmdi zmdi-delete"></i> Delete</button></div>';

            return $output;
        })->make(true); 

        return $table;
    }
}
