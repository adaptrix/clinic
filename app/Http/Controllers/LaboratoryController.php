<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaboratoryController extends Controller
{
    public function index()
    {
        $title = 'Laboratory';
        return view('lab.index',compact('title'));
    }
}
