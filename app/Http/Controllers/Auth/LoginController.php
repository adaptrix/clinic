<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'name';
    }

    public function logout()
    {
        auth()->logout();   
        return redirect('/login');
    }

    public function authenticated(Request $request)
    {
        // Logic that determines where to send the user
        if($request->user()->hasRole('admin')){
            return redirect('/admin');
        }
        if($request->user()->hasRole('doctor')){
            return redirect('/doctor');
        }

        if($request->user()->hasRole('pharmacist')){
            return redirect('/pharmacy');
        }

        if($request->user()->hasRole('laboratorist')){
            return redirect('/laboratory');
        }

        if($request->user()->hasRole('receptionist')){
            return redirect('/reception');
        }
    }
}
