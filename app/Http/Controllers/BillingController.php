<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Billing;
use DB;

class BillingController extends Controller
{
    public function index()
    {
        $title = 'Reception :: Payments';
        return view('reception.billing',compact('title'));
    }

    public function pharmacyBilling()
    {
        $title = 'Pharmacy :: Payments';
        return view('billings.index',compact('title'));
    }

    public function prescriptionBilling()
    {
        $title = 'Pharmacy :: Prescriptions';
        return view('pharmacy.prescription',compact('title'));
    }

    public function store(Request $reaquest)
    {
        $billing = new Billing;
        $billing->transaction_id = $request->transaction_id;
        $billing->patient_reg_no = $request->patient_reg_no;
        $billing->service_id = $request->service_id;
        $billing->amount = $request->amount;
        $billing->amount_paid = $request->amount_paid;
        $billing->amount_due = float_val($request->amount) - float_val($request->dicount);
        $billing->discount = $request->discount;

        $billing->save();

    }

    public function update($id, Request $request)
    {
        $billing = Billing::find($id);
        $billing->transaction_id = $request->transaction_id;
        $billing->patient_reg_no = $request->patient_reg_no;
        $billing->service_id = $request->service_id;
        $billing->amount = $request->amount;
        $billing->amount_paid = $request->amount_paid;
        $billing->amount_due = float_val($request->amount) - float_val($request->dicount);
        $billing->discount = $request->discount;

        $billing->save();
    }

    public function delete($id)
    {
        Billing::destroy($id);
    }

    public function transactionId()
    {
        $nextId = DB::select("show table status like 'billings'");
        $trans_id = 'PAY-'.timestamp().'-'.sprintf("%02d", $nextId[0]->Auto_increment);
        return $trans_id;
    }
}
