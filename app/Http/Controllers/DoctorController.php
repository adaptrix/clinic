<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index()
    {
        $title = 'Consultant';
        return view('doctor.index',compact('title'));
    }
}
