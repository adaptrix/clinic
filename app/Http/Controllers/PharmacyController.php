<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PharmacyController extends Controller
{
    public function index()
    {
        $title = 'Pharmacy :: Medicines';
        return view('pharmacy.index',compact('title'));
    }
}
