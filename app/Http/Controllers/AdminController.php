<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\RoleUser;
use App\Room;
use App\Service;
use App\ServiceMode;
use App\UnitAllocation;

class AdminController extends Controller
{
    public function index()
    {
        $title = 'Ádmininstrator';
        return view('admin.admin',compact('title'));
    }

    //user

    public function addUser(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->employee_id = $request->employee_id;

        $user->save();        
        $role = Role::whereName($request->role)->first();
        $user->roles()->attach($role);

        return response()->json(['data' => 'success']);

    }

    public function updateUser(Request $request)
    {
        if($request->data)
        {
            $id = $request->id;
            $user = User::find($id);
            $role_user = RoleUser::where('user_id',$id)->first();
            $role = Role::find($role_user->role_id);
            return response()->json([
                'userid' => $user->id,
                'employee_id' => $user->employee_id,
                'username' => $user->name,
                'email' => $user->email,
                'role' => $role->name,
                'url' => url('/admin/user/update')
            ]);
        }

        if($request->data == 'delete')
        {
            $id = $request->id;
            User::destroy($id);
            return response()->json(['data' => 'success']);
        }

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password != '')
        {
            $user->password =  bcrypt($request->password);
        }
        
        $user->employee_id = $request->employee_id;

        $user->save();        
        $role = Role::whereName($request->role)->first();
        RoleUser::where('user_id',$request->id)->update(['role_id' => $role->id]);

        return response()->json(['data' => 'success']);
    }

    public function addRoom(Request $request)
    {
        $room = new Room;
        $room->name = $request->room_no;
        $room->room_use = $request->use;
        $room->save();

        return response()->json(['data' => 'success']);

    }

    public function updateRoom(Request $request)
    {
        if($request->data)
        {
            $id = $request->id;
            $room = Room::find($id);
            return response()->json([
                'roomid' => $room->id,
                'room' => $room->name,
                'use' => $room->room_use,
                'url' => url('/admin/room/update')
            ]);
        }

        if($request->data == 'delete')
        {
            $id = $request->id;
            Room::destroy($id);
            return response()->json(['data' => 'success']);
        }

        $room = Room::find($request->id);
        $room->name = $request->room_no;
        $room->room_use = $request->use;
        $room->save();
        return response()->json(['data' => 'success']);
        
    }

    //service

    public function addService(Request $request)
    {
        $service = new Service;
        $service->service_name = $request->service;
        $service->price = $request->price;
        $service->save();

        return response()->json(['data' => 'success']);

    }

    public function updateService(Request $request)
    {
        if($request->data == 'get')
        {
            $id = $request->id;
            $service = Service::find($id);
            return response()->json([
                'serviceid' => $service->service_id,
                'service' => $service->service_name,
                'price' => $service->price,
                'url' => url('/admin/service/update')
            ]);
        }
        
        if($request->data == 'delete')
        {
            $id = $request->id;
            Service::destroy($id);
            return response()->json(['data' => 'success']);
        }

        $service = Service::find($request->id);
        $service->service_name = $request->service;
        $service->price = $request->price;
        $service->save();

        return response()->json(['data' => 'success']);
    }

    //service mode
    public function addServiceMode(Request $request)
    {
        $service_mode = new ServiceMode;
        $service_mode->name = $request->service_mode;
        $service_mode->price = $request->price;
        $service_mode->save();

        return response()->json(['data' => 'success']);
    }

    public function updateServiceMode(Request $request)
    {
        if($request->data == 'get')
        {
            $id = $request->id;
            $servicemode = ServiceMode::find($id);
            return response()->json([
                'modeid' => $servicemode->service_mode_id,
                'servicemode' => $servicemode->name,
                'price' => $servicemode->price,
                'url' => url('/admin/servicemode/update')
            ]);
        }
        
        if($request->data == 'delete')
        {
            $id = $request->id;
            ServiceMode::destroy($id);
            return response()->json(['data' => 'success']);
        }

        $service_mode = ServiceMode::find($request->id);
        $service_mode->name = $request->service_mode;
        $service_mode->price = $request->price;
        $service_mode->save();

        return response()->json(['data' => 'success']);
    }

    //unit allocation
    public function addUnit(Request $request)
    {
        $unit = new UnitAllocation;
        $unit->unit_name = $request->unit;
        $unit->save();

        return response()->json(['data' => 'success']);
    }

    public function updateUnit(Request $request)
    {
        if($request->data == 'get')
        {
            $id = $request->id;
            $unit = UnitAllocation::find($id);
            return response()->json([
                'unitid' => $unit->unit_id,
                'unit' => $unit->unit_name,
                'url' => url('/admin/unit/update')
            ]);
        }
        
        if($request->data == 'delete')
        {
            $id = $request->id;
            UnitAllocation::destroy($id);
            return response()->json(['data' => 'success']);
        }
        
        $unit = UnitAllocation::find($request->id);
        $unit->unit_name = $request->unit;
        $unit->save();

        return response()->json(['data' => 'success']);
    }
}
