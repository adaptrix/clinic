<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitAllocation extends Model
{
    protected $table = 'unit_allocation';
    protected $primaryKey = 'unit_id';
}
