<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    protected $date = ['date','created_at'];
    protected $primaryKey = 'diagnosis_id';
    protected $table = 'diagnosis';
}
