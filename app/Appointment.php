<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $date = ['appointment_date','created_at'];
    protected $primaryKey = 'appointment_id';
}
