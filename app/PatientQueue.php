<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientQueue extends Model
{
    protected $table = 'patient_daily_que';
    protected $date = ['date','created_at','report_time'];
    protected $primaryKey = 'patient_que_id';
}
