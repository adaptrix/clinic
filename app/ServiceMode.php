<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceMode extends Model
{
    protected $table = 'service_modes';
    protected $primaryKey = 'service_mode_id';
}
